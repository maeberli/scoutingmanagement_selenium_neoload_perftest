package ch.hearc.scoutingmanagement.controller.ct.authentication;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.GeneralSecurityException;
import java.util.concurrent.TimeUnit;

import org.apache.olingo.odata2.api.exception.ODataException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.neotys.rest.dataexchange.client.DataExchangeAPIClient;
import com.neotys.rest.dataexchange.client.DataExchangeAPIClientFactory;
import com.neotys.rest.dataexchange.model.ContextBuilder;
import com.neotys.rest.dataexchange.model.TimerBuilder;
import com.neotys.rest.error.NeotysAPIException;
import com.neotys.selenium.proxies.NLWebDriver;
import com.neotys.selenium.proxies.WebDriverProxy;

public class KTAtn1LoginValid {

	private NLWebDriver driver;
	private String baseUrl;
	private StringBuffer verificationErrors = new StringBuffer();

	private DataExchangeAPIClient dataExchangeAPIClient;

	private static final String DATA_1_USERNAME = "laure";
	private static final String DATA_1_PASSWORD = "laure123";

	private static final String DATA_2_USERNAME = "nicolas";
	private static final String DATA_2_PASSWORD = "nicolas123";

	public KTAtn1LoginValid() {
	}

	@Before
	public void setUp() {
		driver = WebDriverProxy.newInstance(new FirefoxDriver());
		baseUrl = "http://10.66.12.19:6543/";
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		final ContextBuilder cb = new ContextBuilder();
		cb.hardware("JenkinsServer").location("SPSCloud").software("ScoutingManagement")
				.script("example script " + System.currentTimeMillis());
		try {
			this.dataExchangeAPIClient = DataExchangeAPIClientFactory
					.newClient("http://localhost:7400/DataExchange/v1/Service.svc/", cb.build(), "apiKeyToSend");
		} catch (GeneralSecurityException | IOException | ODataException | URISyntaxException | NeotysAPIException e) {
			e.printStackTrace();
			org.junit.Assume.assumeNoException(e);
		}
	}

	@Test
	public void testKTAtn1LoginValideData1() {
		driver.setCustomName("KTAtn1LoginValideData1");
		login(DATA_1_USERNAME, DATA_1_PASSWORD);
		assertFalse(driver.findElement(By.cssSelector("body")).getText().matches("^[\\s\\S]*Login error[\\s\\S]*$"));
	}

	@Test
	public void testKTAtn1LoginValideData2() {
		driver.setCustomName("KTAtn1LoginValideData2");
		login(DATA_2_USERNAME, DATA_2_PASSWORD);
		assertFalse(driver.findElement(By.cssSelector("body")).getText().matches("^[\\s\\S]*Login error[\\s\\S]*$"));
	}

	@After
	public void tearDown() {
		driver.quit();
		String verificationErrorString = verificationErrors.toString();
		if (!"".equals(verificationErrorString)) {
			fail(verificationErrorString);
		}
	}

	private void login(String username, String password) {
		final TimerBuilder timer = TimerBuilder.start("Login", "username"+username);
		driver.get(baseUrl + "ScoutingManaging/authentication/login.xhtml");
		driver.findElement(By.name("j_username")).clear();
		driver.findElement(By.name("j_username")).sendKeys(username);
		driver.findElement(By.name("j_password")).clear();
		driver.findElement(By.name("j_password")).sendKeys(password);
		driver.findElement(By.name("submit")).click();

		timer.url(baseUrl + "ScoutingManaging/authentication/login.xhtml");

		try {
			dataExchangeAPIClient.addEntry(timer.stop());
		} catch (GeneralSecurityException | IOException | URISyntaxException | NeotysAPIException e) {
			e.printStackTrace();
			org.junit.Assume.assumeNoException(e);
		}
	}
}
