/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.hearc.scoutingmanagement;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import ch.hearc.scoutingmanagement.controller.ct.authentication.KTAtn1LoginValid;

/**
 *
 * @author marco.aeberli
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
		KTAtn1LoginValid.class})
public class TestSuite {

	public static void main(String args[]) {
		org.junit.runner.JUnitCore.main("ch.hearc.scoutingmanagement.TestSuite");
	}

	@BeforeClass
	public static void setUpClass() throws Exception {
	}

	@AfterClass
	public static void tearDownClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
}